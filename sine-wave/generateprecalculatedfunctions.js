'use-strict';

const generatePrecalculatedFunction = require('./src/generateprecalculatedfunction');

generatePrecalculatedFunction({ name: 'sin', begin: 0, end: 2 * Math.PI, steps: 2048 }, Math.sin);
